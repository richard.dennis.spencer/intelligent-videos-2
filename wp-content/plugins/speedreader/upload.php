<?php
/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <title>Intro Upload</title>
</head>
<body>
<div class="message_platform"></div>
  <?php
    if (isset($_SESSION['message']) && $_SESSION['message'])
    {
    //  printf('<b>%s</b>', $_SESSION['message']);
      echo '<script>document.getElementsByClassName("message_platform")[0].innerText="'.
           $_SESSION['message'].'";</script>';
      unset($_SESSION['message']);
    }
  ?>
  <script>
    function clear_message(){
//      console.log('(EB1)');
      document.getElementsByClassName("message_platform")[0].innerText='';
    }
  </script>
  <style>
	  body
	  {
		  font-family: Arial, Helvetica, sans-serif;
	  }
	  input[type="file"] {
		  border: 1px solid #ccc;
		  display: inline-block;
		  padding: 6px 12px;
		  cursor: pointer;
	  }
	  .message_platform{
		  color: blue;
		  font-weight: bold;
	  }
  </style>
  <h1>Intelligent Videos</h1>
  <h2>Upload Intro</h2>
  <form onsubmit="clear_message()" method="POST" action="upload_intro.php" enctype="multipart/form-data">
    <div>
      <span>Intro file:</span>
      <input type="file" name="uploadedFile" />
      <input type="submit" name="uploadBtn" value="Upload" />
    </div>

  </form>

  <h2>Upload Audio</h2>
  <form onsubmit="clear_message()" method="POST" action="upload_audio.php" enctype="multipart/form-data">
	  <div>
		  <span>Audio file:</span>
		  <input type="file" name="uploadedFile" />
		  <input type="submit" name="uploadBtn" value="Upload" />
	  </div>

  </form>
  <br><br>
  <a href="index.php">Click here to return to Intelligent Videos main page.</a>
  <img src="lifedesignlogo300x153.jpg" alt="CV Logo">

</body>
</html>