<?php
/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/
ini_set('display_errors', 'On');
error_reporting(E_ALL);
if(!session_id()) {
	session_start();
}
$user_dir = $_POST['user_dir'];
$image_path = $_POST['image_path'];
$num_pre_frames = $_POST['num_pre_frames'];
$upload_dir = $user_dir.'/frames/';
$frame_count = $_POST['frame_count'];
for ($i = 0; $i < $num_pre_frames; $i++) {
    $formated_frame_count = sprintf("%'.05d", $frame_count);
    $file = $upload_dir . 'X'. $formated_frame_count . '.png';
    $copy_command = 'cp ' . $image_path . ' ' . $file;
    exec($copy_command);
    $frame_count++;
}
$_SESSION['frame_count'] = $frame_count;
//echo ('((ET181B)'.$frame_count.'|'.$num_pre_frames.'|'.$image_path.'|'.$upload_dir.'|'.$copy_command.')');


