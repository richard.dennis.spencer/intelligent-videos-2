/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/
$(document).ready(function() {

	// process the form
	$('form').submit(function(event) {

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text

		var formData = {
			'filename' 				: $('input[name=filename]').val(),
			'speed_gospel_text' 	: $('input[name=speed_gospel_text]').val(),
			'words_per_minute' 	    : $('input[name=words_per_minute]').val(),
			'font_selection'		: $('input[name=font_selection]').val(),
			'final_text' 			: $('input[name=final_text]').val(),
			'final_text_highlighted': $('input[name=final_text_highlighted]').val(),
			'display_time' 			: $('input[name=display_time]').val(),
			'panel_height' 			: $('input[name=panel_height]').val(),
			'text_pos' 				: $('input[name=text_pos]').val()
		};


		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'wp-content/plugins/speedreader/process.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
			encode 		: true
		})
			.done(function(data) {

				if ( ! data.success) {

					if (data.errors.filename) {
						$('#filename-group').addClass('has-error'); // add the error class to show red input
						$('#filename-group').append('<div class="help-block">' + data.errors.filename + '</div>'); // add the actual error message under our input
					}

					if (data.errors.speed_gospel_text) {
						$('#speed-gospel-text-group').addClass('has-error'); // add the error class to show red input
						$('#speed-gospel-text-group').append('<div class="help-block">' + data.errors.speed_gospel_text + '</div>'); // add the actual error message under our input
					}

					if (data.errors.words_per_minute) {
						$('#speed-gospel-text-group').addClass('has-error'); // add the error class to show red input
						$('#speed-gospel-text-group').append('<div class="help-block">' + data.errors.words_per_minute + '</div>'); // add the actual error message under our input
					}

					if (data.errors.final_text) {
						$('#final-text-group').addClass('has-error'); // add the error class to show red input
						$('#final-text-group').append('<div class="help-block">' + data.errors.final_text + '</div>'); // add the actual error message under our input
					}

					if (data.errors.final_text_highlighted) {
						$('#final-text-highlighted-group').addClass('has-error'); // add the error class to show red input
						$('#final-text-highlighted-group').append('<div class="help-block">' + data.errors.final_text_highlighted + '</div>'); // add the actual error message under our input
					}

					if (data.errors.display_time) {
						$('#display-time-group').addClass('has-error'); // add the error class to show red input
						$('#display-time-group').append('<div class="help-block">' + data.errors.display_time + '</div>'); // add the actual error message under our input
					}

					if (data.errors.panel_height) {
						$('#panel-height-group').addClass('has-error'); // add the error class to show red input
						$('#panel-height-group').append('<div class="help-block">' + data.errors.panel_height + '</div>'); // add the actual error message under our input
					}

					if (data.errors.text_pos) {
						$('#text-pos-group').addClass('has-error'); // add the error class to show red input
						$('#text-pos-group').append('<div class="help-block">' + data.errors.text_pos + '</div>'); // add the actual error message under our input
					}


				} else {

					// ALL GOOD! just show the success message!
					$('#text-pos-group').append('<div class="help-block">' + '(CS3A)' + data.filename + '</div>');

					// usually after form submission, you'll want to redirect
					// window.location = '/thank-you'; // redirect a user to another page

				}
			})

			// using the fail promise callback
			.fail(function(data, textStatus, errorThrown) {

			});

		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});

});