<?php

/**
 * Plugin Name:       Speedreader
 * Plugin URI:        https://hyperbook.info
 * Description:       Speedreader app
 * Version:           0.4.5
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Richard Spencer
 * Author URI:        https://hyperbook.info
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       hyperbook
 * Domain Path:       /
 */

/*
 * todos:


  */

/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/

include ('text_audio.php');
include ('file_table.php');
session_start();
function create_slug($str, $delimiter = '-'){
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;
}

function intelvid() {
	define( 'INTELVID_VERSION', '0.4.5' );
	global $speed_reader_html, $speed_reader_js, $cvg_js, $add_prologue_js, $page_head,
	       $page_neck, $page_foot, $alt_form_head, $alt_form_body, $top, $bottom, $user_dir;

//	echo ('(ET150)');
	$current_user = wp_get_current_user();
	if ( 0 === $current_user->ID ) {
		$user_dir = '/var/www/html/intelvid/anon';
	} else {
		$user_dir = '/var/www/html/intelvid/' . create_slug( $current_user->user_login );
	}
	if ( ! is_dir( $user_dir ) ) {
		mkdir( $user_dir, 0777, true );
		mkdir( $user_dir . '/images', 0777, true );
		mkdir( $user_dir . '/frames', 0777, true );
		mkdir( $user_dir . '/videos', 0777, true );
		mkdir( $user_dir . '/audios', 0777, true );
		mkdir( $user_dir . '/intros', 0777, true );
		mkdir( $user_dir . '/logs', 0777, true );
	}

	$plugins_url = plugins_url();
//	echo '(ET42)'.$plugins_url;

	$alt_form_head = <<<EOD

	<title>Intelligent Videos</title>
<!--	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">  -->


<!--	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">-->
	
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Abril+Fatface|Lato|Old+Standard+TT|Open+Sans|PT+Mono|PT+Serif|Ubuntu|Vollkorn|Tangerine&display=swap" rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> 
	<script src="$plugins_url/speedreader/form_control.js"></script>
	<script src="$plugins_url/speedreader/form_body.js"></script>  
	<script src="$plugins_url/speedreader/form_actions.js"></script> 
	<script src="$plugins_url/speedreader/ajaxq.js"></script> 
	
 </head>

EOD;

	$alt_form_body = <<<EOD

<link rel = "stylesheet" type = "text/css" href = "$plugins_url/speedreader/style.css" />	

<div class="form-style-6">


	<h1>Speedreader</h1>
	  <span id="image_path" hidden>**IMAGEPATH**</span>		
      <form onsubmit="start_generation()">
    	<p></p>
    	<div id="image_upload_group" class="form-group">
			<label for="pre_image_filename">Image file: </label>
			<input id='pre_image_filename_id' type='file' name='pre_image_filename'>   
   		</div>	
    	
   		<div id="image_display_group" class="form-group">
			<label for="use_image">Display image (seconds)</label>
			<input type="number" id="image_display_time" name="image_time" min="0" max="10" value="0">   		
   		</div>
   		<p></p>
		<div id="filename-group" class="form-group">
			<label for="filename">File name</label>
			<input type="text" class="form-control" name="filename" value="s1">
			<!-- errors will go here -->
		</div>

		<div id="speed-gospel-text-group" class="form-group">
			<label for="speed_gospel_text">Intelligent video text</label>
			<input type="text" class="form-control" name="speed_gospel_text">
			<!-- errors will go here -->
		</div>

		<div id="words-per-minute-group" class="form-group">
			<label for="words_per_minute">Words per minute</label>
		<!--	<input type="text" class="form-control" name="words_per_minute" value="350"> -->
			<select name="words_per_minute"><option>50</option><option>75</option><option>100</option><option>125</option>
			<option>150</option><option>175</option><option>200</option><option>225</option><option>250</option>
			<option>275</option><option>300</option><option>325</option><option>350</option><option>375</option>
			<option>400</option><option>425</option><option selected="selected">450</option></select></p>
			<!-- errors will go here -->
		</div> 
		
		<div id="font-group" class="form-group">
			<label for="font_selection">Font</label>
			<select name="font_selection">
				<option class="Arial" selected="selected" value="Arial">Arial</option>
				<option class="Abril-Fatface" value="Abril Fatface">Abril Fatface</option>
				<option class="Lato" value="Lato">Lato</option>
				<option class="Old-Standard-TT" value="Old Standard TT">Old Standard TT</option>
				<option class="Open-Sans" value="Open Sans">Open Sans</option>
				<option class="PT-Mono" value="PT Mono">PT Mono</option>
				<option class="PT-Serif" value="PT Serif">PT Serif</option>
				<option class="Ubuntu" value="Ubuntu">Ubuntu</option>
				<option class="Vollkorn" value="Vollkorn">Vollkorn</option>
				<option class="Tangerine" value="Tangerine">Tangerine</option>
			</select>	
		</div></p>
		
		<div id="font-style-group" class="form-group">
			<label for="font_style">Font style</label>
			<select name="font_style">
				<option class="light" value="lighter">light</option>
				<option class="normal" value="normal" selected="selected">normal</option>
				<option class="bold" value="bolder">bold</option>
			</select>	
		</div>
		
		<div id="text-color-font-group" class="form-group">
			<label for="text_color">Text colour</label>
			<input type="color" id="color-picker-font" name="text_color" value="#000000">
		</div><p>

		<div id="text-color-background-group" class="form-group">
			<label for="background_color">Background colour</label>
			<input type="color" id="color-picker-background" name="backgound_color" value="#bc1d1d">
		</div><p>

		<div id="text-color-highlight-group" class="form-group">
			<label for="hightlight_color">Highlight colour</label>
			<input type="color" id="color-picker-highlight" name="highlight_color" value="#bc1d1d">
		</div><p>

		<div id="final-text-group" class="form-group">
			<label for="final_text">Final text</label>
			<input type="text" class="form-control" name="final_text" value=" ">
			<!-- errors will go here -->
		</div>

        <div id="final-text-highlighted-group" class="form-group">
			<label for="final_text_highlighted">Final text highlighted</label>
			<input type="text" class="form-control" name="final_text_highlighted" value=" ">
			<!-- errors will go here -->
		</div>

        <div id="display-time-group" class="form-group">
			<label for="display_time">Time (in seconds) to display final text</label>
			<input type="text" class="form-control" name="display_time" value="2">
			<!-- errors will go here -->
		</div>

        <div id="panel-height-group" class="form-group">
			<label for="panel_height">Height of coloured panels (%)</label>
			<input type="text" class="form-control" name="panel_height" value="35">
			<!-- errors will go here -->
		</div>

        <div id="text-pos-group" class="form-group">
			<label for="text_pos">Text vertical position adjust(%)</label>
			<input type="text" class="form-control" name="text_pos" value="0.0">
			<!-- errors will go here -->
		</div>

		<button id="generate-button" type="submit">Generate</button>
				
<style type="text/css">
</style>
<div  class="wait_message"></div>
<br>
<div hidden id="loader" class="lds-ring"><div></div><div></div><div></div><div></div></div>

</form>
</div>

<div align="right" id="speed_read_status1"> </div>
	<div class="speed_read_box"></div>
		<canvas class="speed_read_canvas" width="1500" height "150"></canvas>
	</div>
</div>

EOD;

	$page_head = <<< 'EOD'
<!doctype html>
<html>
<head>
EOD;

	$page_neck = <<< 'EOD'
</head>
<body>
EOD;

	$page_foot = <<< 'EOD'
	<!--<script src="https://create.intelligentvideos.co.uk/wp-content/plugins/speedreader/image_upload.js"></script> --> 

</body>
</html>
EOD;

	$top    = $page_head . $alt_form_head . $page_neck;
	$bottom = $page_foot;
	$w      = WP_PLUGIN_DIR;

	$_SESSION['frame_count'] = 0;
	$_SESSION['user_dir']    = $user_dir;
	display_form();
}
	function display_form() {
		global $top, $bottom, $selected_video, $speed_reader_js, $alt_form_body, $user_dir;
//	echo ('(ET153)');
		$directory        = $user_dir . '/images';
		$latest_file_path = latest_file( $directory );
		$last_slash_pos   = strrpos( $latest_file_path, '/' );
		$latest_filename  = substr( $latest_file_path, $last_slash_pos + 1 );
		$afb              = str_replace( '..........', $latest_filename, $alt_form_body );
		$afb              = str_replace( '**IMAGEPATH**', $latest_file_path, $afb );
		$user_dir_div     = '<p></p><div id="user_dir_div" hidden>' . $user_dir . '</div>';
		echo $top . $speed_reader_js .
		     /*		'<a href="<?php echo $edit_post; ?>">Go to New page</a>'.*/
		     $afb . draw_files_list( $selected_video ) .
		     $user_dir_div . '<p>Version: ' . INTELVID_VERSION . '</p>' . $bottom;
	}

	function myStartSession() {
		if ( ! session_id() ) {
			session_start();
		}
	}

	function report_error( $error_code, $form ) {
		var_dump( '(CW2)' );
		var_dump( $error_code );
		var_dump( $form );
	}

	function remove_new_lines( $s ) {
		$sr = str_replace( array( "\r\n", "\r" ), "", $s );

		return $sr;
	}

	function get_file_list( $dir_name ) {
		$file_list = scandir( $dir_name );
//	echo '(ET44A)'.$dir_name.'|'.json_encode($file_list);
		$date_list = array();
		for ( $i = 0; $i < sizeof( $file_list ); $i ++ ) {
			$filename      = $file_list[ $i ];
			$letter_number = '/[0-9a-zA-Z]/';
			if ( 1 === preg_match( $letter_number, $filename ) ) {
				$file_date                = filemtime( $dir_name . '/' . $filename );
				$date_list [ $file_date ] = $file_list[ $i ];
			}
		}
//	echo '(CW72A)'.$dir;
		ksort( $date_list, SORT_NUMERIC );

		return $date_list;
	}

	function latest_file( $dir_name ) {
		$files = glob( $dir_name . "/*" );
		usort( $files, function ( $a, $b ) {
			return filemtime( $a ) < filemtime( $b );
		} );
//	echo('(ET140)');
//	var_dump($files);
		if ( ( isset( $files ) ) && ( count( $files ) > 0 ) ) {
			return $files[0];
		} else {
			return '';
		}
	}

	function latest_file_url( $latest_filename ) {
		global $user_dir;
		$last_slash_pos = strrpos( $user_dir, '/' );
		$username       = substr( $user_dir, $last_slash_pos + 1 );
		$plugins_url    = plugins_url();

		return "$plugins_url/../../intelvid/" . $username . '/images/' . $latest_filename;
	}

	function get_intro_list() {
		$dir = '/var/www/html/intros';
//	$video_dir = "C:\\xampp\\htdocs\\speedgospel\\videos\\";
		$file_list = scandir( $dir );
		$date_list = array();
		for ( $i = 0; $i < sizeof( $file_list ); $i ++ ) {
			$filename      = $file_list[ $i ];
			$letter_number = '/[0-9a-zA-Z]/';
			if ( 1 === preg_match( $letter_number, $filename ) ) {
				$file_date                = filemtime( $dir . '/' . $filename );
				$date_list [ $file_date ] = $file_list[ $i ];
			}
		}
		ksort( $date_list, SORT_NUMERIC );

		return $date_list;
	}

	function select_head( $name, $title ) {
		$rr = '<div id="' . $name . '-select-group" class="form-group">
			<label for="' . $name . '_select">Select ' . $name . '</label>
			<select name="' . $name . '_select">';

		return $rr;
	}

	function select_foot() {
		return '</select></div>';
	}

	function draw_file_table( $directory, $title ) {
		$date_video_list = get_file_list( $directory );
		$list            = '<h2>' . $title . ' List</h2><p>Click filename to download.';
//    $list .= '(EA8H)'.$directory.'|'.$date_video_list[0].'|';
		$list .= '</p><table class="video_list">';
		$list .= '<colgroup><col style="width: 200px"><col style="width: 150px"><col style="width: 75px"><col style="width: 75px"><col style="width: 75px"></colgroup>';
		foreach ( $date_video_list as $d => $f ) {
			$formated_d = date( 'd M Y H:i', $d );
			$rel_dir    = str_replace( 'var/www/html/', '', $directory );
			$list       .= '<tr><td><a href="' . $rel_dir . '/' . $f . '" download>';
			$list       .= $f;
			$list       .= '</a></td><td>' . $formated_d . '</td>' .
			               '<td><form><input type="button" onclick="del_file(\'' . $directory . '\', \'' . $f . '\')" value="Delete"></form></td>' .
			               '</tr>';
		}
		$list .= '</table>';

		return $list;

	}

	function draw_files_list( $selected_video ) {
		global $user_dir;
		$list = draw_file_table( $user_dir . '/videos', 'Video' );
//	$list .= draw_file_table($user_dir . '/intros', 'Intro');
//	$list .= draw_file_table($user_dir . '/audios', 'Audio');
		return $list;
	}

	function replace_quotes_new_lines( $s ) {
//	echo '(CW65)'.$s;
		$s1 = str_replace( '"', '&quot;', $s );
		$s2 = str_replace( "'", '&apos;', $s1 );
		$s3 = str_replace( "\r", ' ', $s2 );
		$s4 = str_replace( "\n", ' ', $s3 );

//	echo '(CW66)'.$s4;
		return $s4;
	}

	$speed_reader_js = <<<'EOD'
  <style>
</style>
EOD;

	function intelvid_upload() {
		include "upload_file.php";
	}

	function intelvid_woocommerce_payment_complete( $order_id ) {
		error_log( "Payment has been received for order $order_id" );
		echo( '(EU10' . $order_id );
		$user_id = get_current_user_id();
		add_user_meta( $user_id, 'user_has_paid', $order_id );
	}

	function text_audio(){
		start_text_audio();
	}

	function download_files(){
		start_file_table();
	}

	add_shortcode( 'intelvid', 'intelvid' );
	add_shortcode( 'intelvid_upload', 'intelvid_upload' );
	add_shortcode( 'text_audio', 'text_audio' );
	add_shortcode( 'download_files', 'download_files' );

//add_filter( 'query_vars', 'addnew_query_vars', 10, 1 );
//add_action( 'woocommerce_payment_complete', 'indelvid_woocommerce_payment_complete', 10, 1 );

?>