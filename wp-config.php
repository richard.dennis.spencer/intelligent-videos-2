<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'intelvid' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Jos9hua9!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~eBtU!Ft&V4;9q#{-AZjWntx|RGbCLjeF(H38P`&$Rp`7Gbs(Q>P*J$jrZ =%b(N' );
define( 'SECURE_AUTH_KEY',  '2u1:6#v&OE9x0K!}nO,qe/;V/Y_`5k-2E;Fe*58o$h1URCyE=P3Psh,m%UQrADD/' );
define( 'LOGGED_IN_KEY',    '<rM7Z#p.{Oyv.RtCXK;BV[*9U;?Y3*`Q,tp_vQ]v8FZR fCer97{%a8f}fdQy6*^' );
define( 'NONCE_KEY',        '[mkh+4sP-]?v%P$W(M?Un5B1jT)KO4!sy650-1FHdcS6xCSt#Y{[A<P5+YRM|E%s' );
define( 'AUTH_SALT',        ',A}rB.|v/Q-R u`]t.Wo9Mgpio#5y-Lk:ml1[Mz`Is>ma8nXBTI@Qx0YdWi`cO-8' );
define( 'SECURE_AUTH_SALT', 'AJ^+7V^/#W8D+GK;SDJ&n/Zw9UBbIyxHf:/^pUT)JH]_[K*vA+vUT]{gIU^Di$C?' );
define( 'LOGGED_IN_SALT',   'a!Y:H/<[yV!0jGvR4qum{0MQ3m4uET43km:~qg$cbJx <0-t/K9*B%4jX&LsW0}E' );
define( 'NONCE_SALT',       'D~kP]YWh:*@^(vc(p!O^?B+s4P=vR4kA{G?dRs)cqaT!+Lw9=SYvw!f{>.d4j3*:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
