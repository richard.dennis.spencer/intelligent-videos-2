<?php
session_start();
$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data

// validate the variables ======================================================
// if any of these variables don't exist, add an error to our $errors array

        if (empty($_POST['filename']))
		$errors['filename'] = 'Filename is required.';

	    if (empty($_POST['speed_gospel_text']))
		$errors['speed_gospel_text'] = 'Text is required.';

        if (empty($_POST['final_text']))
		$errors['final_text'] = 'Final text is required.';

        if (empty($_POST['final_text_highlighted']))
		$errors['final_text_highlighted'] = 'Highlighted text is required.';

        if (empty($_POST['display_time']))
		$errors['display_time'] = 'Display time is required.';

        if (empty($_POST['panel_height']))
		$errors['panel_height'] = 'Panel height is required.';

        if (empty($_POST['text_pos']))
		$errors['text_pos'] = 'Text position is required.';



// return a response ===========================================================

	// if there are any errors in our errors array, return a success boolean of false
	if ( ! empty($errors)) {

		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {

		$form_data_array = array('filename' => $_POST['filename'],
		                         'speed_gospel_text' => $_POST['speed_gospel_text'],
		                         'final_text' => $_POST['final_text'],
		                         'final_text_highlighted' => $_POST['final_text_highlighted'],
		                         'display_time' => $_POST['display_time'],
		                         'panel_height' => $_POST['panel_height'],
		                         'text_pos' => $_POST['text_pos']);
		$_SESSION['form_data'] = json_encode($form_data_array);
		$_SESSION['next_action'] = 'generate';
		// show a message of success and provide a true success variable
		$data['success'] = true;
        $data['message'] = 'Success!';
        echo'(CS11B)'.$_SESSION['form_data'];
	}

	// return all our data to an AJAX call
	echo json_encode($data);