/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/

function del_file(directory, filename) {
  if (!confirm("Delete: "+directory+'/'+filename+"?")) {
    return;
  } reader/back_end_delete.php",
      {filename: filename, directory: directory, user_dir: get_user_dir()})
    .done(function (data) {
      location.reload();
    });
}

function select_video(filename) {
// console.log('(EA17)');
  jQuery.post("wp-content/plugins/speedreader/back_end_select.php",
      {filename: filename, user_dir: get_user_dir()})
    .done(function (data) {
      location.reload();
    });
}

function add_prologue(filename1, filename2){
  // console.log("(CW105)"+filename1+'|'+filename2);
//  sr_add_prologue($filename1, $filename2);

  var data = {
    'action': 'action_function',
    'command': 'add_prologue',
    'filename1': filename1,
    'filename2': filename2,
     user_dir: get_user_dir()
  }
  var data2 = jQuery.post(the_ajax_script.ajaxurl,
    data,
    function () {
      // console.log("(CW106)");
      location.reload();
    })

  jQuery(document.body).trigger('post-load');
}

