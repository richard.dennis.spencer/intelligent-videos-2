<?php
/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/

ini_set('display_errors', 'On');
error_reporting(E_ALL);
if(!session_id()) {
	session_start();
}

if (isset($_POST['imagebase']) ) {
//	echo "(DL2)";
	$frame_count = $_SESSION['frame_count'];
	$user_dir = $_POST['user_dir'];
	$upload_dir = $user_dir.'/frames/';
	$img = json_decode($_POST['imagebase']);
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$formated_frame_count = sprintf("%'.05d", $frame_count);
	$file = $upload_dir . '/X'. $formated_frame_count . '.png';
	$success = file_put_contents($file, $data);
	$_SESSION['frame_count'] = $_SESSION['frame_count'] + 1;

//	echo '(DL3)'.strval($_SESSION['frame_count']);

} else {
//	echo '(DL4)'.strval($_SESSION['frame_count']);
}