<?php
global $text_audio_page;
$text_audio_page = <<<EOD
<html>

<head>
    <script src="https://unpkg.com/wavesurfer.js"></script>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Version 0.2 -->
</head>

<body bgcolor=#EAFAF1>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        .header_line {
                border: none;
                width: 100%;
                overflow: hidden;
                white-space: nowrap;
        }

        #container {
            overflow: scroll;
            width: 1000px;
        }

        #waveform {
            /* float: left; */
            height: 50px;
            width: 1000px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        #d_canvas {
            border: blue solid 1px;
        }

        .text_input {
            width: 300px;
        }

        .coord_input {
            width: 50px;
        }

        .index_input {
            width: 30px;
        }
        
.lds-ring {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
    float: right;
}
.lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #fff;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
}
@keyframes lds-ring {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}

    </style>
    <!-- <div class="header_line"><h2>Audio Text Animation</h2>&nbsp; &nbsp; &nbsp;0.2.1</div> -->
    <div style="float:left;"><h2>Audio Text Animation</h2></div>
    <div style="float:right;">
    <div id="loader" class="lds-ring"></div>
    0.2.5
    </div>
    <div id="waveform" style="float:left;"></div>
    <!-- <div id="time">0</div> -->
    <!-- <div id="events"></div> -->
    <div id="controls" style="float:left;">
        <!-- <button onclick="start_stop()">Start/Stop</button> -->
       
        <div id="color-div-id" class="form-group">
			<label for="fg_color">Foreground colour</label>
			<input type="color" id="fg-color-id" name="fg_color" value="#000000">
		
			<label for="bg_color">Background colour</label>
			<input type="color" id="bg-color-id" name="bg_color" value="#FFFFFF">
		</div>
       
       
        <button class="btn btn-primary" onclick="create_event()">
            <i class="fa fa-comment"></i>
            Create event</button>
        <!-- <button id="off_on" style="display:none">Event</button> -->

        <button class="btn btn-primary" onclick="wavesurfer.skipBackward()">
            <i class="fa fa-step-backward"></i>
            Backward
        </button>

        <button class="btn btn-primary" onclick="wavesurfer.playPause()">
            <i class="fa fa-play"></i>
            Play
            /
            <i class="fa fa-pause"></i>
            Pause
        </button>

        <button class="btn btn-primary" onclick="wavesurfer.skipForward()">
            <i class="fa fa-step-forward"></i>
            Forward
        </button>

        <button class="btn btn-primary" onclick="wavesurfer.toggleMute()">
            <i class="fa fa-volume-off"></i>
            Toggle Mute
        </button>


        <button class="btn btn-primary" onclick="reset()">
            <i class="fa fa-angle-double-left"></i>
            Reset
        </button>

        <label for="file_name_field">Filename:</label>
        <input class="coord_input" type="text" id="file_name_field" value="sample" /> 
        
        <button class="btn btn-primary" onclick="load_data_from_file()">
            <i class="fas fa-folder-open"></i>
            Load data from file
        </button>
        

        <button class="btn btn-primary" onclick="save_data_to_file()">
            <i class="fas fa-save"></i>
             Save data to file
        </button>
 
        <button class="btn btn-primary" onclick="load_sample()">
            <i class="fas fa-badge-check"></i>">
            Load sample data
        </button>
        
        <label for="video_file_name_field">Video filename:</label>
        <input class="coord_input" type="text" id="video_file_name_field" value="a1" /> 

        <button class="btn btn-primary" onclick="generate_video()">
            <i class="fas fa-badge-check"></i>">
            Generate video
        </button>
 
    </form>
  
    </div>
    <div>
        <table id="event_table" width=100%>
            <colgroup>
                <col span="1" style="width: 5%;">
                <col span="1" style="width: 5%;">
                <col span="1" style="width: 56%;">
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 5%;">
                <col span="1" style="width: 5%;">
            </colgroup>
            <thead>
                <tr>
                    <th scope="col">Event</th>
                    <th scope="col">Time</th>
                    <th scope="col">Text</th>
                    <th scope="col">X</th>
                    <th scope="col">Y</th>
                    <th scope="col">Size</th>
                    <th scope="col">Font</th>
                    <th scope="col">Effect</th>
                    <th scope="col">Speed</th>
                    <th scope="col">Clear</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div>
        <canvas id="d_canvas" width="1000" height="500"></canvas>
    </div>
   
    <script>

        // function go(){

        // console.log('(CM5)');
        const d_table_num_columns = 10;
        //var time_display = document.getElementById("time");
        // var off_on_display = document.getElementById("off_on");
        var wave_display = document.getElementById("waveform");
        var events_display = document.getElementById("events");
        var event_table = document.getElementById("event_table");
        var d_canvas = document.getElementById("d_canvas");
        var d_ctx = d_canvas.getContext("2d");
        var file_name_field = document.getElementById("file_name_field");
        var video_file_name_field = document.getElementById("video_file_name_field");
//        var sample_json = '{"event_array":[{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial"},{"event_time":"0.27","event_num":"1","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"I will not leave you as orphans, I will come to you","current_text":"","x":"40","y":"40","size":"M","font":"A","effect":"F","speed":"S","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","canvas_font":"25px Arial"},{"event_time":"4.07","event_num":"2","waveform_x":0,"trig":false,"letter_count":1,"alpha_val":-1,"alpha_count":0,"text":"Before long, the world will not see me any more","current_text":"","x":"80","y":"80","size":"M","font":"T","effect":"L","speed":"M","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","canvas_font":"25px Times"},{"event_time":"10.51","event_num":"3","waveform_x":0,"trig":false,"letter_count":1,"alpha_val":-1,"alpha_count":0,"text":"Because I live, you also will live","current_text":"","x":"160","y":"160","size":"M","font":"A","effect":"L","speed":"F","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"25px Arial"},{"event_time":"8.04","event_num":"4","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"But you will see me","current_text":"","x":"250","y":"120","size":"L","font":"A","effect":"I","speed":"S","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","canvas_font":"40px Arial"},{"event_time":"15.03","event_num":"5","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"On that day, you will realise that I am in my Father and you are in me and I am in you","current_text":"","x":"40","y":"200","size":"S","font":"A","effect":"I","speed":"M","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","canvas_font":"15px Arial"},{"event_time":"21.67","event_num":"6","waveform_x":0,"trig":false,"letter_count":1,"alpha_val":-1,"alpha_count":0,"text":"Whoever has my commands and keeps them is the one who loves me","current_text":"","x":"80","y":"240","size":"S","font":"T","effect":"L","speed":"F","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"15px Times"},{"event_time":"26.94","event_num":"7","waveform_x":0,"trig":false,"letter_count":1,"alpha_val":-1,"alpha_count":0,"text":"The one who loves me will be loved by my Father","current_text":"","x":"120","y":"280","size":"M","font":"T","effect":"L","speed":"F","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"25px Times"},{"event_time":"30.19","event_num":"8","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"I too will love them, and show myself to them","current_text":"","x":"160","y":"320","size":"L","font":"A","effect":"I","speed":"F","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"40px Arial"}],"dummy_splash":{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial"}}';
        var event_time = 99999;
        var event_array = [];
        var focussed_row = -1;
        var last_now = 0.0;
        var frame_count = 0;
        // var linGrad = d_ctx.createLinearGradient(0, 64, 0, 200);
        window.onload = function () {
            reset();
        }
        
function get_user_dir(){
  var user_dir = document.getElementById('user_dir_div').textContent;
  // console.log('(ET43)', user_dir);
  return user_dir;
}
        
jQuery.ajaxq = function (queue, options)
  {
    // Initialize storage for request queues if it's not initialized yet
    if (typeof document.ajaxq == "undefined") document.ajaxq = {q:{}, r:null};

    // Initialize current queue if it's not initialized yet
    if (typeof document.ajaxq.q[queue] == "undefined") document.ajaxq.q[queue] = [];

    if (typeof options != "undefined") // Request settings are given, enqueue the new request
    {
      // Copy the original options, because options.complete is going to be overridden

      var optionsCopy = {};
      for (var o in options) optionsCopy[o] = options[o];
      options = optionsCopy;

      // Override the original callback

      var originalCompleteCallback = options.complete;

      options.complete = function (request, status)
      {
        // Dequeue the current request
        document.ajaxq.q[queue].shift ();
        document.ajaxq.r = null;

        // Run the original callback
        if (originalCompleteCallback) originalCompleteCallback (request, status);

        // Run the next request from the queue
        if (document.ajaxq.q[queue].length > 0) document.ajaxq.r = jQuery.ajax (document.ajaxq.q[queue][0]);
      };

      // Enqueue the request
      document.ajaxq.q[queue].push (options);

      // Also, if no request is currently running, start it
      if (document.ajaxq.q[queue].length == 1) document.ajaxq.r = jQuery.ajax (options);
    }
    else // No request settings are given, stop current request and clear the queue
    {
      if (document.ajaxq.r)
      {
        document.ajaxq.r.abort ();
        document.ajaxq.r = null;
      }

      document.ajaxq.q[queue] = [];
    }
  }
        
            function Splash(event_number, event_time) {
                this.event_time = event_time; // time of event
                // console.log('(CM55)',event_number);
                this.event_num = event_number; // visible number of event
                this.waveform_x = 0; // x-coord of event marker on waveform canvas
                this.trig = false; // true if processing event has been completed
                this.letter_count = -1; // current number of letters of event to be displayed
                this.alpha_val = -1;  // current alpha value for text of event
                this.alpha_count = 0; // used to govern speed of alpha change
                this.text = '';
                this.current_text = this.text;
                this.x = 50;
                this.y = 50;
                this.size = 'M';
                this.font = 'A';
                this.effect = 'I';
                this.speed = 'M';
                this.event_clear_time = event_time + 1;
                this.frame = 0;
                this.letter_frames_step = 10;
                this.alpha_frames_step = 10;
                this.canavs_font = '30px Arial';
                this.completed = false;


                this.load_from_table = function (row_num) {
                    //  console.log('(CM52)','|',row_num,'|','t0t'+row_num.toString());
                    this.event_num = document.getElementById('t0t' + row_num.toString()).value; // visible number of event
                    this.event_time = document.getElementById('t1t' + row_num).value; // time of event
                    this.text = document.getElementById('t2t' + row_num).value;
                    this.x = document.getElementById('t3t' + row_num).value;
                    this.y = document.getElementById('t4t' + row_num).value;
                    this.size = document.getElementById('t5t' + row_num).value;
                    this.font = document.getElementById('t6t' + row_num).value;
                    this.effect = document.getElementById('t7t' + row_num).value;
                    this.speed = document.getElementById('t8t' + row_num).value;
                    this.event_clear_time = document.getElementById('t9t' + row_num).value; // time of clear

                    switch (this.size) {
                        case 'S':
                            this.canvas_font = '15px';
                            break;
                        case 'M':
                            this.canvas_font = '25px';
                            break;
                        case 'L':
                            this.canvas_font = '40px';
                            // console.log('(CM10)','|',this.canvas_font,'|');
                            break;
                    }
                    switch (this.font) {
                        case 'A':
                            this.canvas_font = this.canvas_font + ' Arial';
                            break;
                        case 'T':
                            this.canvas_font = this.canvas_font + ' Times';
                            break;
                        case 'C':
                            this.canvas_font = this.canvas_font + ' Courier';
                            break;
                        default:
                            this.canvas_font = this.canvas_font + ' Arial';
                    }
                    // console.log('(CM21)','|',this.canvas_font,'|');
                    this.letter_count = -1;
                    this.alpha_val = -1;
                    switch (this.effect) {
                        case 'I':
                            break;
                        case 'L':
                            this.letter_count = 1;
                            break;
                        case 'F':
                            this.alpha_val = 0;
                            break;
                    }
                    switch (this.speed) {
                        case 'S':
                            this.letter_frames_step = 40;
                            this.alpha_frames_step = 10;
                            break;
                        case 'M':
                            this.letter_frames_step = 20;
                            this.alpha_frames_step = 5;
                            break;
                        case 'F':
                            this.letter_frames_step = 10;
                            this.alpha_frames_step = 3;
                            break;
                    }
                }

                this.add_to_table = function () {
                    // console.log('(CM1)');
                    var event_prop = this.event_time / wavesurfer.getDuration();
                    this.waveform_x = event_prop * w_canvas.width;
                    //  console.log('(CM2)', this.event_num, this.event_time, event_prop, this.waveform_x);
                    w_ctx.font = "12px Arial";
                    w_ctx.fillStyle = "green";
                    w_ctx.fillText(this.event_num, this.waveform_x - 10, 15);
                    var event_clear_prop = this.event_clear_time / wavesurfer.getDuration();
                    this.waveform_x = event_clear_prop * w_canvas.width;
                    w_ctx.font = "12px Arial";
                    w_ctx.fillStyle = "red";
                    w_ctx.fillText(this.event_num, this.waveform_x - 10, 45);

                    // console.log('(CM120)',this);
                    var event_time_trun = parseFloat(this.event_time).toFixed(2);
                    var event_clear_time_trun = parseFloat(this.event_clear_time).toFixed(2);

                    var row = event_table.insertRow(-1);
                    var cells = [];
                    for (var j = 0; j < d_table_num_columns; j++) {
                        cells[j] = row.insertCell(j);
                    }
                    en = this.event_num;
                    // console.log('(CM54)',en,event_time_trun);
                    cells[0].innerHTML = '<input onfocus="ft(' + en + ')" class="index_input" type="text" id="t0t' + en + '" value="' + en + '"/>';
                    cells[1].innerHTML = '<input onfocus="ft(' + en + ')" class="index_input" type="text" id="t1t' + en + '" value="' + event_time_trun + '"/>';
                    cells[2].innerHTML = '<input onfocus="ft(' + en + ')" class="text_input" type="text" id="t2t' + en + '" value="' + this.text + '"/>';
                    cells[3].innerHTML = '<input onfocus="ft(' + en + ')" class="coord_input" type="text" id="t3t' + en + '" value="' + this.x + '"/>';
                    cells[4].innerHTML = '<input onfocus="ft(' + en + ')" class="coord_input" type="text" id="t4t' + en + '" value="' + this.y + '"/>';
                    // console.log('(CM140)', this.size);
                    cells[5].innerHTML = '<select onfocus="ft(' + en + ')" id="t5t' + en + '">' +
                        '<option value="S">Small</option>' +
                        '<option value="M">Medium</option>' +
                        '<option value="L">Large</option>' +
                        '</select>';
                    var element = document.getElementById('t5t' + en);
                    element.value = this.size;
                    cells[6].innerHTML = '<select onfocus="ft(' + en + ')" id="t6t' + en + '">' +
                        '<option value="A">Arial</option>' +
                        '<option value="T">Times</option>' +
                        '<option value="C">Courier</option>' +
                        '</select>';
                    element = document.getElementById('t6t' + en);
                    element.value = this.font;
                    cells[7].innerHTML = '<select onfocus="ft(' + en + ')" id="t7t' + en + '">' +
                        '<option value="I">Immediate</option>' +
                        '<option value="L">By letter</option>' +
                        '<option value="F">Fade in</option>' +
                        '</select>';
                    element = document.getElementById('t7t' + en);
                    element.value = this.effect;
                    cells[8].innerHTML = '<select onfocus="ft(' + en + ')" id="t8t' + en + '">' +
                        '<option value="S">Slow</option>' +
                        '<option value="M">Medium</option>' +
                        '<option value="F">Fast</option>' +
                        '</select>';
                    element = document.getElementById('t8t' + en);
                    element.value = this.speed;
                    cells[9].innerHTML = '<input onfocus="ft(' + en + ')" class="index_input" type="text" id="t9t' + en + '" value="' + event_clear_time_trun + '"/>';
                }
                this.clone = function (s) {
                    // console.log('(CM132)', s);
                    this.event_time = s.event_time; // time of event
                    this.event_num = s.event_num; // visible number of event
                    this.waveform_x = s.waveform_x; // x-coord of event marker on waveform canvas
                    this.text = s.text;
                    this.current_text = s.text;
                    this.x = s.x;
                    this.y = s.y;
                    this.size = s.size;
                    this.font = s.font;
                    this.effect = s.effect;
                    this.speed = s.speed;
                    this.event_clear_time = s.event_clear_time; // time of event
                }
            }

           /* function get_home_url() {
                var href = window.location.href;
                var index = href.indexOf('/wp-content/plugins/spreedreader');
                var homeUrl = href.substring(0, index);
                console.log('(EV10)', href, homeUrl);
                return homeUrl;
            }*/

            function Splashes() {
                this.event_array = [];
                this.bg_color = "#FFFFFF";
                this.fg_color = "#000000";
                this.dummy_splash = new Splash(0, 0);
                this.event_array.push(this.dummy_splash);
                this.insert = function (splash) {
                    this.event_array.push(splash);
                };
                this.num_splashes = function () {
                    return this.event_array.length;
                };
                this.splash = function (index) {
                    return this.event_array[index];
                };
                this.save_to_file = function (filename) {
                    this.fg_color = document.getElementsByName('fg_color')[0].value;
                    this.bg_color = document.getElementsByName('bg_color')[0].value;
                    this_string = JSON.stringify(this);
                    localStorage.setItem(filename, this_string);
                    // console.log('(CM110A)', filename, this_string);
                }

                this.clone = function (ss) {
                    for (i = 1; i < ss.event_array.length; i++) {
                        s = new Splash(0, 0);
                        this.insert(s);
                        // console.log('(CM130)', ss.event_array);
                        this.event_array[i].clone(ss.event_array[i]);
                        this.event_array[i].add_to_table();
                    }
                    this.fg_color = ss.fg_color;
                    this.bg_color = ss.bg_color;
                }
            }

            var splashes = new Splashes();
            //localStorage.setItem('sample', sample_json);
            var wavesurfer = WaveSurfer.create({
                container: '#waveform',
                scrollParent: false,
                height: 50,
                waveColor: "black",
                progressColor: 'blue',
                cursorColor: 'black',
                barWidth: 1
            });
//            var home_url = get_home_url();
            wavesurfer.load('http://86.134.197.13/wp-content/plugins/speedreader/Joh14vv18to20.mp3');
            console.log('(CM7)', wave_display);
            var w_canvas = wave_display.childNodes[0].childNodes[1];
            var w_ctx = w_canvas.getContext("2d");
            // console.log('(CM4)', w_canvas);
            w = w_canvas.style.width;
            // console.log('(CM3)', w);
            wavesurfer.on('ready', function () {
                wavesurfer.play();
            });

            function hex_to_rgba(hex, alpha) {
                var c;
                if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
                    c = hex.substring(1).split('');
                    if (c.length == 3) {
                        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
                    }
                    c = '0x' + c.join('');
                    return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
                }
                throw new Error('Bad Hex');
            }

            wavesurfer.on('audioprocess', function () {
                var now = wavesurfer.getCurrentTime();
                // console.log('(EV11)',now);
                // time_display.innerHTML = now;
                var fg_color = document.getElementsByName('fg_color')[0].value;
                var bg_color = document.getElementsByName('bg_color')[0].value;
                // console.log('(EV7C)',document.getElementsByName('fg_color')[0].value);
                // console.log(document.getElementsByName('fg_color'));
                for (var i = 1; i < splashes.num_splashes(); i++) {
                    var s = splashes.splash(i);
                    var ttd = s.text;
                    if (!s.completed) {
                        if (!isNaN(s.event_clear_time) && (now > s.event_clear_time)) {
                            ttd = '';
                            d_ctx.fillStyle = bg_color;
                            // console.log('(EV6)', d_ctx.fillStyle, d_canvas.width, d_canvas.height);
                            d_ctx.fillRect(0, 0, d_canvas.width, d_canvas.height);
                            s.completed = true;
                        } else {
                            if (now > s.event_time) {
                                //  console.log('(CM9)',s.trig,i,now,s.event_time,ttd);
                                if (s.trig) {

                                } else {
                                    if (s.letter_count > 0) {
                                        if ((s.frame % s.letter_frames_step) === 0) {
                                            if (s.letter_count > s.text.length) {
                                                s.trig = true;
                                            } else {
                                                s.letter_count++;
                                                ttd = s.text.substring(0, s.letter_count);
                                                // console.log('(CM16)',ttd, s.letter_count);
                                            }
                                            // console.log('(CM17)');
                                            s.frame++;
                                        } else {
                                            s.frame++;
                                            return;
                                        }
                                    }
                                    //  console.log('(CM61)',s.alpha_val,s.alpha_count,s.alpha_frames_step);
                                    if (s.alpha_val >= -0.1) {
                                        // console.log('(CM62)',s.alpha_val,s.alpha_frames_step);
                                        if ((s.alpha_count % s.alpha_frames_step) === 0) {
                                            if (s.alpha_val >= 1.0) {
                                                s.trig = true;
                                            } else {
                                                s.alpha_val = s.alpha_val + 0.01;
                                            }
                                            s.alpha_count++;
                                        } else {
                                            s.alpha_count++;
                                            return;
                                        }
                                    }

                                    //events_display.innerHTML = events_display.innerHTML + '-' + (i + 1);
                                    if (s.alpha_val < -0.1) {
                                        d_ctx.fillStyle = fg_color;
                                    } else {
                                        d_ctx.fillStyle = hex_to_rgba(fg_color, s.alpha_val);
                                        // d_ctx.fillStyle = "rgba(0, 0, 0, " + (s.alpha_val) + ")";
                                    }
                                    d_ctx.strokeStyle = fg_color;
                                    d_ctx.font = s.canvas_font;
                                    if (d_ctx.fillStyle !== fg_color) {
                                        var fs = d_ctx.fillStyle;
                                        d_ctx.fillStyle = bg_color;
                                        d_ctx.fillText(ttd, s.x, s.y);
                                        d_ctx.fillStyle = fs;
                                    }
                                    //  console.log('(CM4)',s.alpha_val,ttd, s.x, s.y, fg_color);
                                    d_ctx.fillText(ttd, s.x, s.y);
                                }
                            }
                        }
                    }
                }
                if (now - last_now > (1/24.0)){
//                       $('.lds-ring').show();
                     var png = JSON.stringify(d_canvas.toDataURL());
			      console.log('(EV14)',frame_count,now);
                    jQuery.ajaxq("speed_gospel_queue", {
                        url: "http://86.134.197.13/wp-content/plugins/speedreader/back_end_generate.php",
                       data: {frame_count: frame_count, imagebase: png, user_dir: get_user_dir()},
                        method: 'POST'
                    })
                    last_now = now;
				    frame_count++;
                }
            });

            function start_stop() {
                wavesurfer.playPause();
                w_ctx.strokeStyle = 'red';
                w_ctx.strokeRect(0, 0, w_canvas.width, w_canvas.height);
            }

            function create_event() {
                event_time = wavesurfer.getCurrentTime();
                event_num = splashes.num_splashes();
                // console.log('(CM51)',splashes.num_splashes(), event_time, event_num);
                s = new Splash(event_num, event_time);
                splashes.insert(s);
                s.add_to_table();
            }

            function reset() {
                wavesurfer.stop();
                splashes = new Splashes();
                num_rows = document.getElementById("event_table").rows.length;
                // console.log('(CM40)', num_rows);
                var fg_color = document.getElementsByName('fg_color')[0].value;
                var bg_color = document.getElementsByName('bg_color')[0].value;
                if (num_rows > 1) {
                    for (i = 1; i < num_rows; i++) {
                        var s = new Splash(i, 0.0);
                        // console.log('(CM41)', i);
                        s.load_from_table(i);
                        splashes.insert(s);
                    }
                }
                d_ctx.fillStyle = bg_color;
                // console.log('(CM45C)', d_ctx.fillStyle, d_canvas.width, d_canvas.height);
                d_ctx.fillRect(0, 0, d_canvas.width, d_canvas.height);
                d_ctx.fillStyle = fg_color;
            }

            d_canvas.addEventListener('click', function (event) {
                var click_x = event.pageX - d_canvas.offsetLeft;
                var click_y = event.pageY - d_canvas.offsetTop;
                // console.log('(CM81)', click_x, click_y, document.activeElement);
                if (focussed_row > 0) {
                    var tx = document.getElementById('t3t' + focussed_row.toString());
                    tx.value = click_x;
                    var ty = document.getElementById('t4t' + focussed_row.toString());
                    ty.value = click_y;
                }
            });

            function ft(row_num) {
                // console.log('(CM82)', row_num);
                if (focussed_row > 0) {
                    var tx = document.getElementById('t3t' + focussed_row.toString());
                    tx.style.backgroundColor = "white";
                    var ty = document.getElementById('t4t' + focussed_row.toString());
                    ty.style.backgroundColor = "white";
                }
                focussed_row = row_num;
                var ttx = document.getElementById('t3t' + focussed_row.toString());
                ttx.style.backgroundColor = "yellow";
                var tty = document.getElementById('t4t' + focussed_row.toString());
                tty.style.backgroundColor = "yellow";
            }

            function loadJSON(callback) {
                var xobj = new XMLHttpRequest();
                xobj.overrideMimeType("application/json");
                xobj.open('GET', 'Sermon.json', true); // Replace 'my_data' with the path to your file
                xobj.onreadystatechange = function () {
                    if (xobj.readyState == 4 && xobj.status == "200") {
                        // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                        speech_to_text = JSON.parse(xobj.responseText)
                        callback(speech_to_text);
                    }
                };
                xobj.send(null);
            }

            empty_table = function () {
                var event_table = document.getElementById("event_table");
                var num_rows = event_table.rows.length;
                // console.log('(EV4)', num_rows);
                if (num_rows > 1) {
                    for (var i = num_rows - 1; i > 0; i--) {
                        event_table.deleteRow(i);
                        // console.log('(EV5)', i);
                    }
                }
            }

            load_data_from_file = function () {
                // console.log('(CM102)');
                empty_table();
                var filename = file_name_field.value;
                splashes = new Splashes();
                json_string = localStorage.getItem(filename);
                //  console.log('(CM111)', filename, json_string);
                var splashes_from_file = JSON.parse(json_string);
                // console.log('(CM112)', splashes_from_file);
                splashes.clone(splashes_from_file);

                document.getElementsByName('fg_color')[0].value = splashes.fg_color;
                document.getElementsByName('bg_color')[0].value = splashes.bg_color;
                // console.log('(CM113A)',splashes.fg_color,splashes.bg_color,splashes);
                reset();
            }

            save_data_to_file = function () {
                reset();
                // console.log('(EV1)',file_name_field.value);
                splashes.save_to_file(file_name_field.value);
            }

            load_sample = function () {
                // var sample_json = '{"event_array":[{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","event_clear_time":1,"frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial"},{"event_time":"0.27","event_num":"1","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"I will not leave you as orphans, I will come to you","current_text":"","x":"40","y":"40","size":"M","font":"A","effect":"F","speed":"S","event_clear_time":"4.00","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","canvas_font":"25px Arial"},{"event_time":"4.07","event_num":"2","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"Before long, the world will not see me any more","current_text":"","x":"80","y":"80","size":"M","font":"T","effect":"F","speed":"M","event_clear_time":"7.00","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","canvas_font":"25px Times"},{"event_time":"10.51","event_num":"3","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"Because I live, you also will live","current_text":"","x":"160","y":"160","size":"M","font":"A","effect":"F","speed":"F","event_clear_time":"14.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"25px Arial"},{"event_time":"8.04","event_num":"4","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"But you will see me","current_text":"","x":"250","y":"120","size":"L","font":"A","effect":"F","speed":"S","event_clear_time":"9.50","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","canvas_font":"40px Arial"},{"event_time":"15.03","event_num":"5","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"On that day, you will realise that I am in my Father and you are in me and I am in you","current_text":"","x":"40","y":"200","size":"S","font":"A","effect":"F","speed":"M","event_clear_time":"20.00","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","canvas_font":"15px Arial"},{"event_time":"21.67","event_num":"6","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"Whoever has my commands and keeps them is the one who loves me","current_text":"","x":"80","y":"240","size":"S","font":"T","effect":"F","speed":"F","event_clear_time":"25.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"15px Times"},{"event_time":"26.94","event_num":"7","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"The one who loves me will be loved by my Father","current_text":"","x":"120","y":"280","size":"M","font":"T","effect":"F","speed":"F","event_clear_time":"29.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"25px Times"},{"event_time":"30.19","event_num":"8","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":0,"alpha_count":0,"text":"I too will love them, and show myself to them","current_text":"","x":"160","y":"320","size":"L","font":"A","effect":"F","speed":"F","event_clear_time":"33.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","canvas_font":"40px Arial"}],"dummy_splash":{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","event_clear_time":1,"frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial"}}';
                var sample_json = '{"event_array":[{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","event_clear_time":1,"frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial","completed":false},{"event_time":"0.27","event_num":"1","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"I will not leave you as orphans, I will come to you","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"S","event_clear_time":"4.00","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"4.07","event_num":"2","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"Before long, the world will not see me any more","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"M","event_clear_time":"7.00","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"10.51","event_num":"3","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"Because I live, you also will live","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"F","event_clear_time":"14.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"8.04","event_num":"4","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"But you will see me","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"S","event_clear_time":"9.50","frame":0,"letter_frames_step":40,"alpha_frames_step":10,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"15.03","event_num":"5","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"On that day, you will realise that I am in my Father and you are in me and I am in you","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"M","event_clear_time":"20.00","frame":0,"letter_frames_step":20,"alpha_frames_step":5,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"21.67","event_num":"6","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"Whoever has my commands and keeps them is the one who loves me","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"F","event_clear_time":"25.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"26.94","event_num":"7","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"The one who loves me will be loved by my Father","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"F","event_clear_time":"29.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"},{"event_time":"30.19","event_num":"8","waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"I too will love them, and show myself to them","current_text":"","x":"88","y":"88","size":"L","font":"A","effect":"I","speed":"F","event_clear_time":"33.00","frame":0,"letter_frames_step":10,"alpha_frames_step":3,"canavs_font":"30px Arial","completed":false,"canvas_font":"40px Arial"}],"bg_color":"#000000","fg_color":"#ffffff","dummy_splash":{"event_time":0,"event_num":0,"waveform_x":0,"trig":false,"letter_count":-1,"alpha_val":-1,"alpha_count":0,"text":"","current_text":"","x":50,"y":50,"size":"M","font":"A","effect":"I","speed":"M","event_clear_time":1,"frame":0,"letter_frames_step":10,"alpha_frames_step":10,"canavs_font":"30px Arial","completed":false}}';
                localStorage.setItem('sample', sample_json);
            }

			generate_video = function () {
				render(video_file_name_field.value);
			}

			function render(filename) {
			     console.log('(DT61C)',frame_count,filename,get_user_dir());
			    jQuery.ajaxq("speed_gospel_queue", {
			      url: "http://86.134.197.13/wp-content/plugins/speedreader//back_end_render.php",
			      data:  {filename: filename, user_dir: get_user_dir()},
			      method: 'POST',
			      success: function(data){
			         console.log('(ET70)'+data);
//			        $('.lds-ring').hide();
			        //$('.wait_message').text('');
			       location.reload();
			      }}
			    )}

            fill_table = function (stt) {
                // console.log('(CM101)',stt);
                var items = stt.results.items;
                var p = '';
                var event_time = -1.0;
                for (var i = 0; i < 400; i++) {
                    // console.log('(CM107)',items[i]);
                    var word = items[i].alternatives[0].content;
                    if (items[i].type === 'pronunciation') {
                        p = p + ' ' + word;
                        // console.log('(CM106)',p);
                        if (event_time < 0) {
                            event_time = parseFloat(items[i].start_time);
                        }
                        // console.log('(CM104)',word);
                    } else {
                        p = p + word;
                        var event_num = splashes.num_splashes();
                        s = new Splash(event_num, event_time);
                        splashes.insert(s);
                        s.text = p;
                        s.add_to_table();
                        event_time = -1.0;
                        p = '';
                        // console.log('(CM105)',p,event_num, event_time);
                    }
                }
            }
        
    </script>

    
</body>

</html>
EOD;
function start_text_audio(){
    global $text_audio_page;
	$current_user = wp_get_current_user();
	if ( 0 === $current_user->ID ) {
		$user_dir = '/var/www/html/intelvid/anon';
	} else {
		$user_dir = '/var/www/html/intelvid/' . create_slug( $current_user->user_login );
	}
	echo '<div id="user_dir_div" hidden>' . $user_dir . '</div>';
    echo $text_audio_page;
};

?>