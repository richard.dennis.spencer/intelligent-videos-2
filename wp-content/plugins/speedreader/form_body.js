/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/
  var frame_count = 0;
  var frames_completed = 0;
  var selected_video = '';
  // console.log('(DT101)');
  $(window).on("load", function() {
    // console.log('(DT102)');
    $('.lds-ring').hide();
    // draw_pre_image();
  })

/*
function drawDataURIOnCanvas(strDataURI, ctx) {
  "use strict";
  var img = new window.Image();
  img.addEventListener("load", function () {
    ctx.drawImage(img, 0, 0);
  });
  img.setAttribute("src", strDataURI);
}
*/

/*
function draw_pre_image() {
  console.log('(ET170A)');
  var con = document.getElementsByClassName('speed_read_canvas')[0];
  var ctx = con.getContext("2d");
  var image_url = document.getElementById("image_url").innerText;
  if (0 !== parseInt(image_display_time)) {
    drawDataURIOnCanvas(image_url, ctx);
    console.log('(ET171)');
  }
}
*/


 /* $('#font-selection > option').hover(function() {
    console.log('(ET22)');
    $(this).parent().css({fontFamily:$(this).val()})
  })
*/

  jQuery.ajaxq = function (queue, options)
  {
    // Initialize storage for request queues if it's not initialized yet
    if (typeof document.ajaxq == "undefined") document.ajaxq = {q:{}, r:null};

    // Initialize current queue if it's not initialized yet
    if (typeof document.ajaxq.q[queue] == "undefined") document.ajaxq.q[queue] = [];

    if (typeof options != "undefined") // Request settings are given, enqueue the new request
    {
      // Copy the original options, because options.complete is going to be overridden

      var optionsCopy = {};
      for (var o in options) optionsCopy[o] = options[o];
      options = optionsCopy;

      // Override the original callback

      var originalCompleteCallback = options.complete;

      options.complete = function (request, status)
      {
        // Dequeue the current request
        document.ajaxq.q[queue].shift ();
        document.ajaxq.r = null;

        // Run the original callback
        if (originalCompleteCallback) originalCompleteCallback (request, status);

        // Run the next request from the queue
        if (document.ajaxq.q[queue].length > 0) document.ajaxq.r = jQuery.ajax (document.ajaxq.q[queue][0]);
      };

      // Enqueue the request
      document.ajaxq.q[queue].push (options);

      // Also, if no request is currently running, start it
      if (document.ajaxq.q[queue].length == 1) document.ajaxq.r = jQuery.ajax (options);
    }
    else // No request settings are given, stop current request and clear the queue
    {
      if (document.ajaxq.r)
      {
        document.ajaxq.r.abort ();
        document.ajaxq.r = null;
      }

      document.ajaxq.q[queue] = [];
    }
  }

  function get_user_dir(){
    var user_dir = document.getElementById('user_dir_div').textContent;
    // console.log('(ET43)', user_dir);
    return user_dir;
  }


  function add_frame(canvas, frames_to_display_final) {
    $('.lds-ring').show();
    $('.wait_message').text('Please wait, do not navigate away from or refresh this window.');
    var png = JSON.stringify(canvas.toDataURL());
     // console.log('(ET44)',get_user_dir(),frame_count);
    jQuery.ajaxq("speed_gospel_queue", {
      url: "wp-content/plugins/speedreader/back_end_generate.php",
      data: {frame_count: frame_count, imagebase: png, user_dir: get_user_dir()},
      method: 'POST'
    })
    var progress_bar = document.getElementById('progress_bar');
  //  progress_bar.value = (frame_count * 50.0) / frames_to_display_final;
  //  console.log('(DT51A)',frame_count, progress_bar.value);
    frame_count++;
  }



  function render(filename) {
    // console.log('(DT61C)',frame_count,filename,get_user_dir());
    jQuery.ajaxq("speed_gospel_queue", {
      url: "wp-content/plugins/speedreader/back_end_render.php",
      data:  {filename: filename, user_dir: get_user_dir()},
      method: 'POST',
      success: function(data){
        // console.log('(ET70)'+data);
        $('.lds-ring').hide();
        $('.wait_message').text('');
       location.reload();
      }}
    )}

function add_intro_audio() {
    var filename = document.getElementsByName('video_select')[0].value;
    var intro = document.getElementsByName('intro_select')[0].value;
    var audio = document.getElementsByName('audio_select')[0].value;
    var comb_filename = document.getElementsByName('comb_filename')[0].value;
    if (!confirm("Add "+intro+" and "+audio+" to "+filename+" to make "+comb_filename+"?")) {
      return;
    }
    $('.lds-ring').show();
    $('.wait_message').text('Please wait, do not navigate away from or refresh this window.');
    // console.log('(DT71)',filename, intro, audio, comb_filename);
    jQuery.ajaxq("speed_gospel_queue", {
      url: "wp-content/plugins/speedreader/back_end_intro_audio.php",
      data:  {filename: filename,
              intro: intro,
              audio: audio,
              comb_filename: comb_filename,
              user_dir: get_user_dir()},
      method: 'POST',
      success: function(data){
        $('.lds-ring').hide();
        $('.wait_message').text('');
        // console.log('(DT111)',data)
        location.reload();
      }}
    )}
function set_canvas_font(font_size){
  var con = document.getElementsByClassName('speed_read_canvas')[0];
  var ctx = con.getContext("2d");
  var font_selection = document.getElementsByName("font_selection")[0].value;
  var font_style = document.getElementsByName("font_style")[0].value;
  ctx.font = font_style + " " + font_size + "px " + font_selection;//"px Arial";
}

function start_generation(){
   // console.log('(CW80)')
  frame_count = -1;
  frames_completed = 0;
  // console.log('(CW800)',frame_count,frames_completed);

  var filename = document.getElementsByName("filename")[0].value;
  var speed_gospel_text = document.getElementsByName("speed_gospel_text")[0].value;
  var words_per_minute = parseInt(document.getElementsByName("words_per_minute")[0].value);
  var final_text = document.getElementsByName("final_text")[0].value;
  var final_text_highlighted = document.getElementsByName("final_text_highlighted")[0].value;
  var display_time = parseInt(document.getElementsByName("display_time")[0].value);
  var panel_height = parseInt(document.getElementsByName("panel_height")[0].value);
  var text_pos = parseFloat(document.getElementsByName("text_pos")[0].value);
  var text_color = document.getElementsByName("text_color")[0].value;
  var backgound_color = document.getElementsByName("backgound_color")[0].value;
  var highlight_color = document.getElementsByName("highlight_color")[0].value;
  var pre_image_filename = document.getElementById("pre_image_filename_id").innerText;
  var image_display_time = document.getElementById("image_display_time").value;
  var num_pre_frames = parseInt(image_display_time) * 24;
  var image_path = document.getElementById("image_path").innerText;

   // console.log('(ET121D)',image_filename,image_display_time,image_path);
  if (words_per_minute < 50) words_per_minute = 300;
  var video_fps = 24;
  var msec_per_word = (60 * 1000) / words_per_minute;
  var words_per_second = words_per_minute / 60.0;
  var frames_per_word = video_fps / words_per_second;
  var frames_to_display_final = display_time * video_fps;
  // console.log( '(CW81)'+frames_per_word);

//	var window_call = 'speed_window=window.open("","","width=2000,height=1100,menubar=no,statusbar=no,titlebar=no");';
//	var html = file_get_contents(plugins_url('speedreaderhtmlfrag.html', __FILE__ ));
//<<	var speed_gospel_text = "'.replace_quotes_new_lines($speed_gospel_text).'";
//<<		          'var final_text ="'.replace_quotes_new_lines($final_text).'";'.
//<<		          'var final_text_high ="'.replace_quotes_new_lines($final_text_highlighted).'";'.





  /*console.log('(CX90C)',file_name,speed_gospel_text,words_per_second,
    msec_per_word,final_text, final_text_high, frames_to_display_final,
    height_of_coloured_panels,'>', text_v_pos_adjust, frames_per_word);*/
  var complete = 'X';
  var tc = text_color;
  var rc = backgound_color;
  var hc = highlight_color;
  var bars_percent = parseInt(panel_height);
  var bar_color = rc;
  var bar_border_percent = 2;
  var pointer_height_percent = 5;
  var link_time = 10; //seconds
  var box = document.getElementsByClassName('speed_read_box')[0];
  var font_size = 130;
  var ms_per_word = parseInt(msec_per_word);//184;
  // console.log('(CW90)', speed_gospel_text);
  var in_text = speed_gospel_text.replace(/&quot;/g, '"');
  in_text = in_text.replace(/&apos;/g, "'");
  var words = in_text.split(' ');
  // console.log('(CW82)'+words);
  var pos = 0;
  var con = document.getElementsByClassName('speed_read_canvas')[0];
  var ctx = con.getContext("2d");
  var back_img = document.getElementById("wp-content/plugins/speedreader/background-image");
  con.width  = 1920;
  con.height = 1080;
  con.style.width  = '1920px';
  con.style.height = '1080px';
  set_canvas_font(font_size);

  // console.log('(ET21)'+ctx.font);
  var y_adjust = (parseFloat(text_pos) * con.height) / 100.0;
  var ypos = (con.height + font_size) /2 - 20 + y_adjust;

// in_text = 'a bb ccc dddd'
// console.log('(DK30E)',text_v_pos_adjust,y_adjust,ypos, in_text, ctx.font);

  function sleep(miliseconds) {
    var currentTime = new Date().getTime();
    while (currentTime + miliseconds >= new Date().getTime()) {
    }
  }

  function run_script(){
     // console.log('(ET100)');
    if (jQuery.ajaxq.isRunning("speed_gospel_queue")) {
       alert('Intelligent Videos in use.  Please wait at least 5 minutes, then try again.');
      $('.lds-ring').hide();
      $('.wait_message').text('');
      return;
    }
    copy_pre_image_file();

  }

  function generate_pre_image (){
    // console.log('(ET180A)',frame_count, get_user_dir(), num_pre_frames, image_path);
    if (num_pre_frames > 0) {
      jQuery.ajaxq("speed_gospel_queue", {
        url: "wp-content/plugins/speedreader/back_end_generate_pre_image.php",
        data: {
          frame_count: frame_count,
          user_dir: get_user_dir(),
          num_pre_frames: num_pre_frames,
          image_path: image_path
        },
        method: 'POST',
        success: function (data) {
          // console.log('(ET179)¬' + data);
          display_text();
        }
      })
      frame_count = num_pre_frames;
    } else {
      display_text();
    }
  }

  function copy_pre_image_file(){
    pre_image_filename = document.getElementById("pre_image_filename_id").files[0].name;
    console.log('(EU1C)',pre_image_filename, get_user_dir());

    jQuery.ajaxq("speed_gospel_queue", {
      url: "wp-content/plugins/speedreader/copy_pre_image.php",
      data: {user_dir: get_user_dir(), pre_image_filename:  pre_image_filename},
      method: 'POST',
      success: function(data) {
         console.log('(EU2)' + data);
        clear_images();
      }
    })

  }

  function clear_images() {
    frame_count = 0;
    // console.log('(DT41)');
    jQuery.ajaxq("speed_gospel_queue", {
      url: "wp-content/plugins/speedreader/back_end_clear_images.php",
      data: {user_dir: get_user_dir()},
      method: 'POST',
      success: function(data) {
         // console.log('(ET31A)' + data);
        generate_pre_image();
      }
    })
  }

  function display_text(){
    //   ctx.fillText("6Hello World", 10, 50);
    // console.log('(ET120B)');
    /*for (var i=0; i<24; i++){
      add_frame(con);
    }
*/    var id = setInterval(frame, ms_per_word);
    var alpha = 1.0;
    // draw_pre_image();
    function frame() {
      if (pos >= words.length){
        clearInterval(id);
        var start_fading = 0.33;
        var fade_frame_start = start_fading * frames_to_display_final;
        var end_fading = 0.66;
        var fade_frame_end = end_fading * frames_to_display_final;
        for (let i=0;i<(frames_to_display_final);i++){
          // console.log('(EA1)',i);
          if (i < (fade_frame_start)){
            alpha = 1.0;
          } else {
            if (i < (fade_frame_end)){
              alpha = 1.0 - ((i - fade_frame_start) / (fade_frame_end - fade_frame_start));
              // console.log('(DK69E)',frames_to_display_final,i,
              //   fade_frame_start,fade_frame_end,alpha);
            } else {
              alpha = 0.0;
            }
          }
          draw_background(alpha);
          draw_final_text();
          add_frame(con, frames_to_display_final);

        }
         // console.log('(EA2)',frames_completed,frame_count);

         render(filename);
         // console.log('(EA3)',filename);

        show_progress();
      }  else {
        word = words[pos]
        wl = word.length;
        if (wl == 0){
          // console.log('(DK2)',word,wl)

        } else {
          if (wl == 1){
            // console.log('(DK3)',word,wl)
            var before = '';
            highlight = word;
            after = '';
          } else {
            if (wl == 2){
              before = word[0];
              highlight = word[1];
              after = '';
            } else {
              hw = Math.floor(wl/2);
              before = word.substring(0,hw);
              // console.log('(DK31)',word,before)
              //                             before_width = measureText(before, 120);
              //                              bl = (box_left_pos - before_width)+'px';

              highlight = word[hw];
              after = word.substring(hw+1);
            }

          }

        }
        // console.log('(DK4)',before,highlight,after);
        draw_background(1);
        let before_length =  ctx.measureText(before,font_size).width;
        let highlight_half_length = ctx.measureText(highlight, font_size).width/2;
        let before_pos = con.width/2 - (before_length + highlight_half_length);
        //ctx.clearRect(0,0,con.width,con.height);
        //con.style.left = (box_left_pos - before_length - highlight_half_length) + 'px';
        //  console.log('(DK45)',before,highlight,after,before_length,highlight_half_length,con.style.left);
        let highlight_pos = con.width/2 - highlight_half_length;
        var hw = ctx.measureText(highlight).width;
        ctx.fillStyle = tc;
        ctx.fillText(before, before_pos, ypos);
        ctx.fillStyle = hc;
        ctx.fillText(highlight, highlight_pos, ypos);
        ctx.fillStyle = tc;
        ctx.fillText(after, highlight_pos + highlight_half_length*2, ypos);

        ctx.fillStyle = "rgba(32, 45, 21, 0.3)";
        set_canvas_font(150);
        ctx.strokeText('intelligentvideos.co.uk', 50, con.height / 3);

        //               console.log('(DK20)',hh, before_length, highlight_half_length)
      }

      pos++;
      for (let j=0; j<frames_per_word; j++){
        add_frame(con, frames_to_display_final);
      }
    }
  }

  function draw_final_text() {
    fs1 = font_size * 0.66;
    fs2 = Math.floor(fs1);
    fs3 = fs2.toString();
    smaller_font = (Math.floor(font_size * 0.66)).toString();
    // console.log('(DK7A)',fs1,fs2,fs3,smaller_font);
    set_canvas_font(smaller_font);
    ctx.font = smaller_font;
    var total_final_text = final_text + final_text_highlighted;
    var lw = ctx.measureText(total_final_text).width;
    ctx.fillStyle = 'black';
    var final_text_x = con.width/2 - lw/2;
    var high_text_x = final_text_x + ctx.measureText(final_text).width;
    // console.log('(DK37G)',final_text_x,high_text_x,Math.floor(font_size * 0.66),ctx.font);
    ctx.fillText(final_text, final_text_x, ypos);
    ctx.fillStyle = rc;
    ctx.fillText(final_text_highlighted, high_text_x, ypos);
    ctx.fillStyle = 'black';
  }

  function draw_background(alpha){
    faded_black = 'rgba(0,0,0,' + alpha.toString() + ')';
    faded_color = adjustHexAlpha(bar_color, alpha);
    // console.log('(DK6A)',faded_color);
    ctx.fillStyle ='white';
    ctx.fillRect(0, 0, con.width, con.height);
    ctx.fillStyle = faded_color;
    let top_bar_bottom = con.height * bars_percent / 100.0;
    ctx.fillRect(0, 0, con.width, top_bar_bottom);
    ctx.fillStyle = faded_black;
    let border_height = con.height * bar_border_percent / 100.0;
    let pointer_height = con.height * pointer_height_percent / 100.0;
    ctx.fillRect(0, top_bar_bottom, con.width, border_height);
    ctx.fillRect(con.width/2 - border_height/2,  top_bar_bottom + border_height,
      border_height, pointer_height);
    ctx.fillStyle = faded_color;
    let bottom_bar_top = con.height - (con.height * bars_percent / 100.0);
    ctx.fillRect(0, bottom_bar_top, con.width, con.height);
    ctx.fillStyle = faded_black;
    ctx.fillRect(0, bottom_bar_top - border_height, con.width, border_height);
    ctx.fillRect(con.width/2 - border_height/2,  bottom_bar_top - pointer_height - border_height,
      border_height, pointer_height);
  }

  function adjustHexAlpha (color, alpha) {
    const r = parseInt(color.slice(1, 3), 16);
    const g = parseInt(color.slice(3, 5), 16);
    const b = parseInt(color.slice(5, 7), 16);

    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
  }


  function report_complete(){
    // console.log(('(DT5'));
    setTimeout(refresh_window, 10000);
  }

  function refresh_window(){
    location.reload();
  }

  function show_progress() {
    // console.log('(DT7)');
  }

  function sr_add_prologue(filename1, filename2){
    cvg.add_prologue(filename1, filename2);
  }

  run_script();
}

