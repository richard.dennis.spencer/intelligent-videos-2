<?php
function get_files_list( $dir_name ) {
	$file_list = scandir( $dir_name );
//	echo '(ET44A)'.$dir_name.'|'.json_encode($file_list);
	$date_list = array();
	for ( $i = 0; $i < sizeof( $file_list ); $i ++ ) {
		$filename      = $file_list[ $i ];
		$letter_number = '/[0-9a-zA-Z]/';
		if ( 1 === preg_match( $letter_number, $filename ) ) {
			$file_date                = filemtime( $dir_name . '/' . $filename );
			$date_list [ $file_date ] = $file_list[ $i ];
		}
	}
//	echo '(CW72A)'.$dir;
	ksort( $date_list, SORT_NUMERIC );

	return $date_list;
}


function draw_files_table( $directory, $title ) {
	$current_user = wp_get_current_user();
	if ( 0 === $current_user->ID ) {
		$user_dir = '/var/www/html/intelvid/anon';
	} else {
		$user_dir = '/var/www/html/intelvid/' . create_slug( $current_user->user_login );
	}
	$plugins_url = plugins_url();
	$list = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js'></script>";
	$list .= "<script src='$plugins_url/speedreader/form_body.js'></script>";
	$list .= "<script src='$plugins_url/speedreader/form_actions.js'></script>";
	$list .= "<script src='$plugins_url/speedreader/ajaxq.js'></script>";
	$list .= "<p></p><div id='user_dir_div' hidden>" . $user_dir . "</div>";
	$date_video_list = get_files_list( $directory );
	$list .= '<h2>' . $title . ' List</h2><p>Click filename to download.';
//    $list .= '(EA8H)'.$directory.'|'.$date_video_list[0].'|';
	$list .= '</p><table class="video_list">';
	$list .= '<colgroup><col style="width: 200px"><col style="width: 150px"><col style="width: 75px"><col style="width: 75px"><col style="width: 75px"></colgroup>';
	foreach ( $date_video_list as $d => $f ) {
		$formated_d = date( 'd M Y H:i', $d );
		$rel_dir    = str_replace( 'var/www/html/', '', $directory );
		$list       .= '<tr><td><a href="' . $rel_dir . '/' . $f . '" download>';
		$list       .= $f;
		$list       .= '</a></td><td>' . $formated_d . '</td>' .
		               '<td><form><input type="button" onclick="del_file(\'' . $directory . '\', \'' . $f . '\')" value="Delete"></form></td>' .
		               '</tr>';
	}
	$list .= '</table>';
	return $list;
}


function start_file_table() {
	$current_user = wp_get_current_user();
	if ( 0 === $current_user->ID ) {
		$user_dir = '/var/www/html/intelvid/anon';
	} else {
		$user_dir = '/var/www/html/intelvid/' . create_slug( $current_user->user_login );
	}
	$list = draw_files_table( $user_dir . '/videos', 'Video' );
	echo $list;
}