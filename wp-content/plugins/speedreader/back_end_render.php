<?php
/*
Copyright 2019 Hyperbook Ltd. All rights reserverd
*/

ini_set('display_errors', 'On');
error_reporting(E_ALL);
if(!session_id()) {
	session_start();
}
$user_dir = $_POST['user_dir'];
if (isset($_POST['filename']) ) {
//	echo "(DL62)";
	$filename = $_POST['filename'];
	$path = '"'.sprintf('%s/%s.mp4', $user_dir . '/videos', $filename).'"';
	$frame_dir = $user_dir . '/frames/';
	$log_dir = $user_dir . '/logs/';
	$ffmpeg = 'ffmpeg -progress pipe:1 -r 24 -start_number 0 -fflags +genpts -y -v quiet '
              .'-i ' . $frame_dir. 'X%05d.png -refs 5 '.
	          '-c:v libx264 -preset slow -f mp4 -pix_fmt yuv420p -strict -2 -r 24 '.
	            $path. ' 1> ' . $log_dir . 'log1.log 2> ' . $log_dir . 'log2.log';
	$t3 = shell_exec($ffmpeg);
//    echo "(ET9)".$t3.'|'.$path.'|'.$ffmpeg;
}